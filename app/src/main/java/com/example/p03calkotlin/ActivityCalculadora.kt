package com.example.p03calkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog


class ActivityCalculadora : AppCompatActivity() {
    private lateinit var btnSumar: Button;
    private lateinit var btnRestar: Button;
    private lateinit var btnMultiplicar: Button;
    private lateinit var btnDividir: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnRegresar: Button;

    private lateinit var txtNum1: EditText;
    private lateinit var txtNum2: EditText;
    private lateinit var lblResultado: TextView;
    private var calculadora = Calculadora(0,0);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()
        btnSumar.setOnClickListener { btnSumar() }
        btnRestar.setOnClickListener { btnRestar() }
        btnMultiplicar.setOnClickListener { btnMultiplicar() }
        btnDividir.setOnClickListener { btnDividir() }
        btnLimpiar.setOnClickListener { limpiar() }
        btnRegresar.setOnClickListener { regresar() }
    }

    fun limpiar(){
        txtNum1.setText("")
        txtNum2.setText("")
        lblResultado.setText("")
    }

    fun regresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿ Desea regresar ?")
        confirmar.setPositiveButton("Confirmar"){dialogoInterface,wich->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogoInterface,wich->}.show()

    }

    private fun iniciarComponentes(){
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        lblResultado = findViewById(R.id.lblResultado)
    }

    private fun btnSumar() {
        val num1Text = txtNum1.text.toString()
        val num2Text = txtNum2.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1 = num1Text.toInt()
            calculadora.num2 = num2Text.toInt()
            lblResultado.text = calculadora.suma().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnRestar() {
        val num1Text = txtNum1.text.toString()
        val num2Text = txtNum2.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1 = num1Text.toInt()
            calculadora.num2 = num2Text.toInt()
            lblResultado.text = calculadora.resta().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnMultiplicar() {
        val num1Text = txtNum1.text.toString()
        val num2Text = txtNum2.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1 = num1Text.toInt()
            calculadora.num2 = num2Text.toInt()
            lblResultado.text = calculadora.multiplicacion().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnDividir() {
        val num1Text = txtNum1.text.toString()
        val num2Text = txtNum2.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1 = num1Text.toInt()
            calculadora.num2 = num2Text.toInt()
            lblResultado.text = calculadora.division().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

}