package com.example.p03calkotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar:Button
    private lateinit var btnCerrar:Button
    private lateinit var txtUsuario:EditText
    private lateinit var txtContrasena:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar.setOnClickListener { ingresar() }
        btnCerrar.setOnClickListener { cerrar() }
    }

    private fun iniciarComponentes(){
        btnIngresar = findViewById(R.id.btnIngresar)
        btnCerrar = findViewById(R.id.btnCerrar)
        txtContrasena = findViewById(R.id.txtContrasena)
        txtUsuario = findViewById(R.id.txtUsuario)
    }

    private fun ingresar(){
        var strUsuario:String
        var strContra:String
        //Asignar los strings que se declararon en R.Values (como sacar lo que declare en la carpeta de values)
        strUsuario = application.resources.getString(R.string.usuario)
        strContra = application.resources.getString(R.string.contrasena)

        if(txtUsuario.text.toString() == strUsuario && txtContrasena.text.toString() == strContra){
            //Hacer el paquete de datos que se van a envíar
            var bundle = Bundle();
            bundle.putString("usuario", strUsuario)

            val intent = Intent(this@MainActivity, ActivityCalculadora::class.java)   //Intent es un servicio
            intent.putExtras(bundle)                                        //
            startActivity(intent)

        }else{
            Toast.makeText(this.applicationContext,"Usuario o contraseña no válidos", Toast.LENGTH_LONG).show()
        }

    }

    private fun cerrar(){
        finish();
    }
}